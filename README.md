# GGMU - SporTna Tacka

Projektni zadatak iz predmeta Principi softverskog inzenjerstva


## Uputstvo za pokretanje aplikacije lokalno

Zahtevi:
 - docker
 - php7
 - composer

```bash
git clone https://gitlab.com/DavidT58/ggmu-sportna-tacka.git
cd ggmu-sportna-tacka
docker-compose up -d
cd code-sportna-tacka
composer install
```

## Uputstvo za pokretanje migracija i seedera baze
```bash
docker exec -it app bash -c "php spark migrate" #migracija
```

## Uputstvo za dodavanje novih funkcionalnosti/karakteristika

Na sledecem [linku](https://gitlab.com/DavidT58/ggmu-sportna-tacka/-/boards) otvorite novi tiket u "Open" koloni i dodate mu kratak opis; nakon toga, pritisnete na njega i napravite novi **branch** (bez merge requesta) od master grane

```bash
git checkout 1-test-grana
~pisanje koda~
git add .
git commit -m "<commit message>"
git push origin 1-test-grana
git checkout dev # predjemo na dev granu da bi u njega merge-ovali novu funkcionalnost
git pull origin dev # povucemo najnoviju verziju dev grane sa servera
git merge --no-ff --no-commit 1-test-grana # u slucaju merge conflicta konsultovati se sa timom
git commit -m "#1 to dev" # umesto #1 staviti broj tiketa/grane
git push origin dev
```
