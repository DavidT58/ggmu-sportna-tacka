function prikaziRezultat() {
    let brPitanja = document.getElementById('brPitanja').value;
    console.log(brPitanja);
    let skor = 0;
    for (let i = 0; i < brPitanja; i++) {
        let brOdgovora = document.getElementById('pitanje' + i).value;
        console.log(brOdgovora);
        for (let j = 0; j < brOdgovora; j++) {
            let radioDugme = document.getElementById('brPitanja' + i + 'odg' + j);
            if (radioDugme.checked && radioDugme.value == 1)
                skor += 1;
        }
    }
    document.getElementById("Rezultat").innerHTML = "<h2>Broj na tačno odgovorenih pitanja je " + skor + "</h2>";
}