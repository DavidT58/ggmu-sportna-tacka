
function prikaziFormeZaPitanja() {
    let sel = document.getElementById('brojPitanja');
    let brojPitanja = sel.options[sel.selectedIndex].value;
    let i;
    let htmlCode = "<table width='100%'>";
    for (i = 0; i < brojPitanja; i++) {
        htmlCode += "<tr><td>Unesite tekst pitanja broj " + (i + 1) + ":</td>";
        htmlCode += "<td><textarea name='pitanje"+i+"' cols='30' rows='2' required></textarea></td></tr>";
        
        htmlCode += "<tr><td colspan='2' id='odgovori" + i + "'>Unesite broj odgovora koji želite:";
        htmlCode += "<select style='margin-left:80px;' id='brOdgovora" + i + "' onchange='prikaziFormeZaOdgovore("+i+")' required><option value='.'>_</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option></select></td></tr>";
    }
    htmlCode += "</table>";
    document.getElementById('forma').innerHTML = htmlCode;
}

function prikaziFormeZaOdgovore(broj) {
    let selO = document.getElementById('brOdgovora' + broj);
    let brojOdgovora = selO.options[selO.selectedIndex].value;

    let htmlCode = "<table width='100%'>";
    htmlCode += "<input type='hidden' name='brOdgZaPitanje"+(broj)+"' value='"+ brojOdgovora +"'></input>";
    for (let i = 0; i < brojOdgovora; i++) {
        htmlCode += "<tr><td>Unesite " + (i + 1) + ". ponuđeni odgovor:";
        let sumaOdg = 0;
        for (let k = 0; k < broj; k++) {
          sumaOdg += parseInt(document.getElementsByName('brOdgZaPitanje' + k)[0].value);  
        }
        htmlCode += "<input style='margin-left:50px;' type='text' name='odgovor"+(i+sumaOdg)+"' required><label style='margin-left:80px;'>Čekirajte odgovor ukoliko je tačan:</label><input style='margin-left:5px;' type='radio' name='radioGrupa"+broj+"' id='radioGrupa"+broj+"odg"+i+"' value='radioGrupa"+broj+"odg"+i+"' required></td></tr>";
    }
    htmlCode += "</table>"
    document.getElementById('odgovori' + broj).innerHTML = htmlCode;
}