<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MainSeeder extends Seeder{
	public function run(){
        $this->call('SportSeeder');
        $this->call('KorisnikSeeder');
        $this->call('ClanakSeeder');
        $this->call('ForumSekcijaSeeder');
	}
}
