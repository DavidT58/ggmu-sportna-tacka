<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SportSeeder extends Seeder{
	public function run(){
		$data = [
            'imeSporta'      => "Fudbal",
        ];
		$this->db->table('Sport')->insert($data);

        $data = [
            'imeSporta'      => "Kosarka",
        ];
		$this->db->table('Sport')->insert($data);

        $data = [
            'imeSporta'      => "Tenis",
        ];
		$this->db->table('Sport')->insert($data);

        $data = [
            'imeSporta'      => "Formula1",
        ];
		$this->db->table('Sport')->insert($data);
	}
}
