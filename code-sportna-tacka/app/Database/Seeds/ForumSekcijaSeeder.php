<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ForumSekcijaSeeder extends Seeder{
	public function run(){
		$data = [
            'nazivSekcije'      => "Pravila foruma"
        ];
		$this->db->table('ForumSekcija')->insert($data);

        $data = [
            'nazivSekcije'      => "Fudbal"
        ];
		$this->db->table('ForumSekcija')->insert($data);

        $data = [
            'nazivSekcije'      => "Kosarka"
        ];
		$this->db->table('ForumSekcija')->insert($data);

        $data = [
            'nazivSekcije'      => "Tenis"
        ];
		$this->db->table('ForumSekcija')->insert($data);

        $data = [
            'nazivSekcije'      => "Formula 1"
        ];
		$this->db->table('ForumSekcija')->insert($data);
	}
}
