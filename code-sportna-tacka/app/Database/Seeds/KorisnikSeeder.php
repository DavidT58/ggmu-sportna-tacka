<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KorisnikSeeder extends Seeder{
	public function run(){
		$data = [
            'username'      => "admin",
            'datumRodjenja' => "2021-01-01",
            'email'         => "admin@admin.com",
            'password'      => "admin",
            'tip'           => 2,
            'ban'           => 0
        ];
		$this->db->table('Korisnik')->insert($data);

        $data = [
            'username'      => "moderator",
            'datumRodjenja' => "2021-01-01",
            'email'         => "moderator@moderator.com",
            'password'      => "moderator",
            'tip'           => 1,
            'ban'           => 0
        ];
		$this->db->table('Korisnik')->insert($data);

        $data = [
            'username'      => "user",
            'datumRodjenja' => "2021-01-01",
            'email'         => "user@user.com",
            'password'      => "user",
            'tip'           => 0,
            'ban'           => 0
        ];
		$this->db->table('Korisnik')->insert($data);
	}
}
