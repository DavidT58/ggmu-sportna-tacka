<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Korisnik extends Migration{

	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idKorisnik' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'username' => [
				'type' => 'VARCHAR',
				'constraint' => 30,
				'unique' => true
			],
			'datumRodjenja date',
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 45
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 30
			],
			'tip' => [
				'type' => 'INT',
				'default' => 0
			],
			'ban' => [
				'type' => 'INT',
				'default' => 0
			]
		]);
		$this->forge->addPrimaryKey('idKorisnik');
		$this->forge->createTable('Korisnik', TRUE);
	}

	public function down(){
		$this->forge->dropTable('Korisnik', TRUE);
	}
}
