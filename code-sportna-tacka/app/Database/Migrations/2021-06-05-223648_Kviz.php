<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Kviz extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idKviz' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idSport' => [
				'type' => 'INT'
			],
			'naslovKviza' => [
				'type' => 'VARCHAR',
				'constraint' => 100
			]
		]);
		$this->forge->addPrimaryKey('idKviz');
		// $this->forge->addPrimaryKey('idTema');
		$this->forge->addForeignKey('idSport', 'Sport', 'idSport', 'CASCADE', 'CASCADE');
		$this->forge->createTable('Kviz', TRUE);
	}

	public function down(){
		$this->forge->dropTable('Kviz', TRUE);
	}
}
