<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Clanak extends Migration{
	public function up(){
		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idClanak' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idSport' => [
				'type' => 'INT'
			],
			'idKorisnik' => [
				'type' => 'INT'
			],
			'naslov' => [
				'type' => 'VARCHAR',
				'constraint' => 150
			],
			'sadrzaj TEXT',
			'datum DATE',
			'naslovnaSlika' => [
				'type' => 'VARCHAR',
				'constraint' => 1000
			]
		]);
		$this->forge->addPrimaryKey('idClanak');
		$this->forge->addForeignKey('idSport', 'Sport', 'idSport');
		$this->forge->addForeignKey('idKorisnik', 'Korisnik', 'idKorisnik');
		$this->forge->createTable('Clanak', TRUE);

	}

	public function down(){
		$this->forge->dropTable('Clanak', TRUE);
	}
}
