<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ForumSekcija extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idSekcija' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'nazivSekcije' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			]
		]);
		$this->forge->addPrimaryKey('idSekcija');
		$this->forge->createTable('ForumSekcija', TRUE);
	}

	public function down(){
		$this->forge->dropTable('ForumSekcija', TRUE);
	}
}
