<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Komentar extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idKomentar' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idClanak' => [
				'type' => 'INT'
			],
			'idKorisnik' => [
				'type' => 'INT'
			],
			'sadrzaj' => [
				'type' => 'VARCHAR',
				'constraint' => 250
			],
			'brUpvote' => [
				'type' => 'INT',
				'default' => 0
			],
			'brDownvote' => [
				'type' => 'INT',
				'default' => 0
			]
		]);
		$this->forge->addPrimaryKey('idKomentar');
		$this->forge->addForeignKey('idClanak', 'Clanak', 'idClanak', 'CASCADE', 'CASCADE');
		$this->forge->addForeignKey('idKorisnik', 'Korisnik', 'idKorisnik', 'CASCADE', 'CASCADE');
		$this->forge->createTable('Komentar', TRUE);
	}

	public function down(){
		$this->forge->dropTable('Komentar', TRUE);
	}
}
