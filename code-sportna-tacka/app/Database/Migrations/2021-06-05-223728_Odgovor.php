<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Odgovor extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idOdgovor' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idPitanje' => [
				'type' => 'INT'
			],
			'tekstOdgovora' => [
				'type' => 'VARCHAR',
				'constraint' => 200
			],
			'tacan' => [
				'type' => 'INT'
			]
		]);
		$this->forge->addPrimaryKey('idOdgovor');
		// $this->forge->addPrimaryKey('idTema');
		$this->forge->addForeignKey('idPitanje', 'Pitanje', 'idPitanje', 'CASCADE', 'CASCADE');
		$this->forge->createTable('Odgovor', TRUE);

		$seeder = \Config\Database::seeder();
		$seeder->call('MainSeeder');
	}

	public function down(){
		$this->forge->dropTable('Odgovor', TRUE);
	}
}
