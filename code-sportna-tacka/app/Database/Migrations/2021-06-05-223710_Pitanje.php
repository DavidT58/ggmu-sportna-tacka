<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pitanje extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idPitanje' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idKviz' => [
				'type' => 'INT'
			],
			'tekstPitanja' => [
				'type' => 'VARCHAR',
				'constraint' => 200
			]
		]);
		$this->forge->addPrimaryKey('idPitanje');
		// $this->forge->addPrimaryKey('idTema');
		$this->forge->addForeignKey('idKviz', 'Kviz', 'idKviz', 'CASCADE', 'CASCADE');
		$this->forge->createTable('Pitanje', TRUE);
	}

	public function down(){
		$this->forge->dropTable('Pitanje', TRUE);
	}
}
