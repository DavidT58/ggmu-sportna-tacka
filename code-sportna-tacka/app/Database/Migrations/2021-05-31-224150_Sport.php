<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Sport extends Migration{

	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idSport' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'imeSporta' => [
				'type' => 'VARCHAR',
				'constraint' => 30
			]
		]);
		$this->forge->addPrimaryKey('idSport');
		$this->forge->createTable('Sport', TRUE);
	}

	public function down(){
		$this->forge->dropTable('Sport', TRUE);
	}
}
