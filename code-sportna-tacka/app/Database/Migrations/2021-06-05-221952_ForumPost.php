<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ForumPost extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idPost' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idTema' => [
				'type' => 'INT'
			],
			'sadrzaj' => [
				'type' => 'TEXT',
				'constraint' => 1000
			],
			'idKorisnik' => [
				'type' => 'INT'
			],
			'brUpvote' => [
				'type' => 'INT',
				'default' => 0
			],
			'brDownvote' => [
				'type' => 'INT',
				'default' => 0
			],
			'username' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			]
		]);
		$this->forge->addPrimaryKey('idPost');
		// $this->forge->addPrimaryKey('idTema');
		$this->forge->addForeignKey('idTema', 'ForumTema', 'idTema', 'CASCADE', 'CASCADE');
		$this->forge->addForeignKey('idKorisnik', 'Korisnik', 'idKorisnik', 'CASCADE', 'CASCADE');
		$this->forge->createTable('ForumPost', TRUE);
	}

	public function down(){
		$this->forge->dropTable('ForumPost', TRUE);
	}
}
