<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ForumTema extends Migration{
	public function up(){

		$forge = \Config\Database::forge();

		$this->forge->addField([
			'idTema' => [
				'type' => 'INT',
				'auto_increment' => true
			],
			'idSekcija' => [
				'type' => 'INT'
			],
			'nazivTeme' => [
				'type' => 'VARCHAR',
				'constraint' => 45
			]
		]);
		$this->forge->addPrimaryKey('idTema');
		// $this->forge->addPrimaryKey('idSekcija');
		$this->forge->addForeignKey('idSekcija', 'ForumSekcija', 'idSekcija', 'CASCADE', 'CASCADE');
		$this->forge->createTable('ForumTema', TRUE);
	}

	public function down(){
		$this->forge->dropTable('ForumTema', TRUE);
	}
}
