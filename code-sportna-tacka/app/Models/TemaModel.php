<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * TemaModel
 * 
 * Andrija Lazarevic 2018/0584
 * 
 * V1.0
 */
class TemaModel extends Model{

        protected $table      = 'ForumTema';
        protected $primaryKey = 'idTema';
        protected $returnType = 'object';

}
