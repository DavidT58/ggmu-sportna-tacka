<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * PostModel
 * 
 * Andrija Lazarevic 2018/0584
 * 
 * V1.0
 */
class PostModel extends Model{

        protected $table      = 'ForumPost';
        protected $primaryKey = 'idPost'; 
        protected $returnType = 'object';
        
        protected $allowedFields = ['idTema', 'sadrzaj', 'idKorisnik', 'username', 'brUpvote', 'brDownvote'];

}