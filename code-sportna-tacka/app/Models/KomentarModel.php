<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * KomentarModel
 * 
 * David Cejtlin 2018/0700
 * Pavle Colic 2018/0103
 * 
 * V1.0
 */
class KomentarModel extends Model{
    protected $table = 'Komentar';
    protected $primaryKey = 'idKomentar';
    
    protected $allowedFields = ['idClanak', 'idKorisnik', 'sadrzaj', 'brUpvote', 'brDownvote'];
    
    public function dohvatiKomentare($idClanak){
        return $this->where('idClanak', $idClanak)->findAll();
    }
}