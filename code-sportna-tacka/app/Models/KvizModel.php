<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * KvizModel
 * 
 * Aleksa Janjic 2018/0554
 * 
 * V1.0
 */
class KvizModel extends Model
{
        protected $table      = 'Kviz';
        protected $primaryKey = 'idKviz';
        protected $returnType = 'object';

        public function pronadjiKviz($idSport) {
                $db = \Config\Database::connect();
                $builder = $db->table('Kviz');
                $builder->where('idSport', $idSport);
                $kviz = $builder->get()->getResult();
                return $kviz;
        }

        public function pronadjiKvizSaNaslovom($idSport, $naslovKviza) {
                $db = \Config\Database::connect();
                $builder = $db->table('Kviz');
                $builder->where('idSport', $idSport);
                $builder->where('naslovKviza', $naslovKviza);
                $kviz = $builder->get()->getResult();
                return $kviz;
        }

        public function insertKviz($selectedSport, $naslovKviza) {
                $data = [
                        'idSport'=> $selectedSport,
                        'naslovKviza'=> $naslovKviza,
                ];
                $db = \Config\Database::connect();
                $builder = $db->table('Kviz');
                $builder->insert($data);
        }
}