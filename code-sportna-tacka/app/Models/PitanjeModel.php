<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\KvizModel;

/**
 * PitanjeModel
 * 
 * Aleksa Janjic 2018/0554
 * 
 * V1.0
 */
class PitanjeModel extends Model
{
        protected $table      = 'Pitanje';
        protected $primaryKey = 'idPitanje';
        protected $returnType = 'object';

        public function pronadjiPitanja($idKviz) {
            $pitanja = [];
            $pitanja = $this->where('idKviz', $idKviz)->findAll();
            return $pitanja;
        }

        public function pronadjiPitanje($idSport, $naslovKviza, $tekstPitanja) {
            $kvizModel = new KvizModel();
            $db = \Config\Database::connect();
            $builder = $db->table('Pitanje');
            $kviz = $kvizModel->pronadjiKvizSaNaslovom($idSport, $naslovKviza);
            $builder->where('idKviz', $kviz[count($kviz)-1]->idKviz);
            $builder->where('tekstPitanja', $tekstPitanja);
            $pitanja = $builder->get()->getResult();
            return $pitanja[count($pitanja)-1];
        }

        public function insertPitanje($tekstPitanja, $naslovKviza, $idSport) {
            $kvizModel = new KvizModel();
            $kviz = $kvizModel->pronadjiKvizSaNaslovom($idSport, $naslovKviza);

            $data = [
                'idKviz'=> $kviz[count($kviz)-1]->idKviz,
                'tekstPitanja'=> $tekstPitanja,
            ];
            $db = \Config\Database::connect();
            $builder = $db->table('Pitanje');
            $builder->insert($data);
        }
}