<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * KorisnikModel
 * 
 * David Cejtlin 2018/0700
 * 
 * V1.0
 */
class KorisnikModel extends Model{
    protected $table = 'Korisnik';
    protected $primaryKey = 'idKorisnik';
    
    protected $allowedFields = ['username', 'datumRodjenja', 'email', 'password', 'tip', 'ban'];

    public function getByUsername($username){
        return $this->where('username', $username)->first();
    }

    
    
}