<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * SekcijaModel
 * 
 * Andrija Lazarevic 2018/0584
 * 
 * V1.0
 */
class SekcijaModel extends Model{

        protected $table      = 'ForumSekcija';
        protected $primaryKey = 'idSekcija';
        protected $returnType = 'object';
        protected $allowedFields = ['nazivSekcije'];


        public function pretraga() {
                return $this->findAll();     
            }
       
        
}

