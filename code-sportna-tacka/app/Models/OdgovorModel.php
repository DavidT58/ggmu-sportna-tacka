<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\PitanjeModel;

/**
 * OdgovorModel
 * 
 * Aleksa Janjic 2018/0554
 * 
 * V1.0
 */
class OdgovorModel extends Model
{
        protected $table      = 'Odgovor';
        protected $primaryKey = 'idOdgovor';
        protected $returnType = 'object';

        public function pronadjiOdgovore($idPitanje) {
            $odgovori = [];
            $odgovori = $this->where('idPitanje', $idPitanje)->findAll();
            return $odgovori;
        }

        public function insertOdgovore($naslovKviza, $idSport, $tekstPitanja, $odgovor, $tacnostOdgovora) {
            $pitanjeModel = new PitanjeModel();
            $pitanje = $pitanjeModel->pronadjiPitanje($idSport, $naslovKviza, $tekstPitanja);
            $data = [
                'idPitanje'=> $pitanje->idPitanje,
                'tekstOdgovora'=> $odgovor,
                'tacan' => $tacnostOdgovora,
            ];
            $db = \Config\Database::connect();
            $builder = $db->table('Odgovor');
            $builder->insert($data);
        }
}