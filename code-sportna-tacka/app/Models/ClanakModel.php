<?php namespace App\Models;

use CodeIgniter\Model;


/**
 * ClanakModel
 * 
 * David Cejtlin 2018/0700
 * Pavle Colic 2018/0103
 * 
 * V1.0
 */
class ClanakModel extends Model{
        protected $table      = 'Clanak';
        protected $primaryKey = 'idClanak';
        protected $returnType = 'object';
        protected $allowedFields = ['sadrzaj'];
        
        public function pretraga($tekst) {
            return $this->like('sadrzaj', $tekst)->orLike('naslov', $tekst)->findAll();     
        }
        
        public function dohvatiVestiAutora($autor) {
            return $this->where('autor', $autor)->findAll();      
        }

        public function insertClanak($idSport, $idKorisnik, $naslovClanka, $tekstClanka) {
            $data = [
                'idSport'=> $idSport,
                'idKorisnik'=> $idKorisnik,
                'naslov' => $naslovClanka,
                'naslovnaSlika' => "",
                'sadrzaj' => $tekstClanka,
                'datum' =>  date("Y/m/d"),
            ];
            $db = \Config\Database::connect();
            $builder = $db->table('Clanak');
            $builder->insert($data);
        }

        public function postaviNaslovnuSliku($idClanak, $filePath) {
            $db = \Config\Database::connect();
            $builder = $db->table('Clanak');
            $builder->set('naslovnaSlika', $filePath);
            $builder->where('idClanak', $idClanak);
            $builder->update();
        }

        public function dohvatiPoslednjiClanak() {
            $db = \Config\Database::connect();
            $builder = $db->table('Clanak');
            $clanci = $builder->get()->getResult();
            return $clanci[count($clanci)-1];
        }

        public function dohvatiPoslednjeClanke($idSport, $limit){
            $db = \Config\Database::connect();

            if($limit <= 0){
                $query = "SELECT * FROM Clanak WHERE idSport = $idSport ORDER BY idClanak DESC";
            }
            else{
                $query = "SELECT * FROM Clanak WHERE idSport = $idSport ORDER BY idClanak DESC LIMIT $limit";
            }

            $ret = $db->query($query);

            // log_message('error', print_r($ret, true));

            $array = [];

            $i = 0;
            foreach($ret->getResult() as $row){
                $array[$i] = $row;
                $i++;
            }

            return $array;
        }
}