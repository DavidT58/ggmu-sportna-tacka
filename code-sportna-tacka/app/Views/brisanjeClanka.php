<div class='container'>

    <div class='row'>
        <table class='table table-dark table-striped'>
            <th>Naslov clanka</th>
            <th>Obrisi</th>
            <?php
                foreach($clanci as $clanak){
                    echo "<tr><td>";
                    echo $clanak->naslov;
                    echo "</td><td>";
                    $idClanak = $clanak->idClanak;
                    echo form_open("Moderator/obrisiClanak/$idClanak");
                    $buttonAtrributes = [ 'class' => 'btn btn-danger'];
                    echo form_submit('OBRISI', 'OBRISI', $buttonAtrributes);
                    echo form_close();
                    echo "</td></tr>";
                }
            ?>
        </table>
    </div>

</div>