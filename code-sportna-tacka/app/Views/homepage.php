
<div class='container'>
  
  <div class='row'>
      <div class='col-sm-12 naslovniClanak'>
        <?php 
          echo "<h1>$glavniClanak->naslov</h1>";
          $idClanak = $glavniClanak->idClanak;
          $anchorAttributes = [
            'class' => 'procitaj-vise'
          ];
          echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
          echo "<img src='$glavniClanak->naslovnaSlika' alt='' style='margin-top: 10px;'>";
        ?>
      </div>
  </div>

  <div class='row'>
    <!-- fudbal -->
    <div class='col-9'>
      <h1 class='title'> Novosti iz sveta fudbala </h1>

      <?php
        $id = 0;
        foreach($clanciFudbal as $clanak){
          echo "<div class='col-4 floatleft'><div class='pocetna-clanci'>";
          $naslov = $clanak->naslov;
          $naslovnaSlika = $clanak->naslovnaSlika;
          echo "<img src='$naslovnaSlika'>";
          echo "<h4>$naslov</h4>";
          $idClanak = $clanak->idClanak;
          $anchorAttributes = [
            'class' => 'procitaj-vise'
          ];
          echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
          echo "</div></div>";
        }
      ?>
    </div>


    <!-- Kosarka -->
    <div class='col-9'>
      <h1 class='title'> Novosti iz sveta kosarke </h1>
      <?php
        $id = 0;
        foreach($clanciKosarka as $clanak){
          echo "<div class='col-4 floatleft d-flex justify-content-around'><div class='pocetna-clanci'>";
          $naslov = $clanak->naslov;
          $naslovnaSlika = $clanak->naslovnaSlika;
          echo "<img src='$naslovnaSlika'>";
          echo "<h4>$naslov</h4>";
          $idClanak = $clanak->idClanak;
          $anchorAttributes = [
            'class' => 'procitaj-vise'
          ];
          echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
          echo "</div></div>";
        }
      ?>
      </div>
    </div>


    <!-- Tenis -->
    <div class='col-9'>
      <h1 class='title'> Novosti iz sveta tenisa </h1>
      <?php
          $id = 0;
          foreach($clanciTenis as $clanak){
            echo "<div class='col-4 floatleft d-flex justify-content-around'><div class='pocetna-clanci'>";
            $naslov = $clanak->naslov;
            $naslovnaSlika = $clanak->naslovnaSlika;
            echo "<img src='$naslovnaSlika'>";
            echo "<h4>$naslov</h4>";
            $idClanak = $clanak->idClanak;
            $anchorAttributes = [
              'class' => 'procitaj-vise'
            ];
            echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
            echo "</div></div>";
          }
        ?>
    </div>

    <!-- F1 -->
    <div class='col-9'>
      <h1 class='title'> Novosti iz sveta Formule 1 </h1>
      <?php
          $id = 0;
          foreach($clanciF1 as $clanak){
            echo "<div class='col-4 floatleft d-flex justify-content-around'><div class='pocetna-clanci'>";
            $naslov = $clanak->naslov;
            $naslovnaSlika = $clanak->naslovnaSlika;
            echo "<img src='$naslovnaSlika'>";
            echo "<h4>$naslov</h4>";
            $idClanak = $clanak->idClanak;
            $anchorAttributes = [
              'class' => 'procitaj-vise'
            ];
            echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
            echo "</div></div>";
          }
        ?>
    </div>
  </div>

<!-- </div> -->
