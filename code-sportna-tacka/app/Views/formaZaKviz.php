
    <script src="/assets/js/formaZaKviz.js"></script>
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-sm-12'>
                <form action="<? echo site_url("$controller/upisiKvizUBazu") ?>" method="POST">
                    <table width="100%" class='table formaZaKviz'>
                        <tr>
                            <td>Izaberite sport za koji želite da postavite kviz?</td>
                            <td>
                                <select name="Sport" required>
                                    <option value="1">Fudbal</option>
                                    <option value="2">Košarka</option>
                                    <option value="3">Tenis</option>
                                    <option value="4">Formula 1</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Unesti naslov kviza:</td>
                            <td>
                                <input type="text" name="Naslov" id="" required>
                            </td>
                        </tr>
                        <tr>
                            <td>Unesite broj pitanja koje želite da kviz ima:</td>
                            <td>
                            <select id="brojPitanja" name="BrojPitanja" onchange="prikaziFormeZaPitanja()" required>
                                <option value=".">_</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td id='forma' colspan='2'>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button type="submit" class="btn btn-dark" value="postaviKviz">Postavi kviz</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>