<div class='content_area'>

    <div class="main_content floatleft">
        <?php
            echo "<h1 class='clanak-naslov'>".$clanak->naslov."</h1>";
            echo img(array('src' => $clanak->naslovnaSlika, 'class' => 'clanak-slika'));
            echo "<br>";
            echo "<div class='clanak-sadrzaj'>";
            echo nl2br($clanak->sadrzaj);
            echo "</div>";
            echo "<br>";

            $imageProperties =[
                'src'    => '/slikeTemp/avatarIcon.png',
                'class'  => 'slika'
            ];

            echo "<h2 class='title'>Komentari</h2>";
            echo "<div class='komentari'>";
            foreach($komentari as $komentar){
                echo "<div class='komentar'>";
                echo "<div class='korisnik-detalji'>";
                echo "<div class=''>";
                echo img($imageProperties);
                echo "</div>";
                echo "<div class=''>";
                echo $komentar['username'];
                echo "</div></div>";
                echo "<div class='sadrzaj'>";
                echo $komentar['sadrzaj'];
                echo "</div></div>";
            }

            echo "</div>";
        ?>
    </div>