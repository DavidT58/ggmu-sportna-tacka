<html>
    <head>
        <title>SporTna Tacka</title>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>

        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<? echo base_url('assets/font/font-awesome.min.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<? echo base_url('assets/font/font.css');?>"/>
        
        <link rel="stylesheet" type="text/css" href="<? echo base_url('assets/css/style.css');?>" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<? echo base_url('assets/css/responsive.css');?>" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<? echo base_url('assets/css/jquery.bxslider.css');?>" media="screen"/>
    </head>
    <body>
        <div class="body_wrapper">
            <div class="center">
                <div class="header_area">
                    <div class="logo floatleft">
                        <?
                            $img = array('src' => 'assets/css/images/logo_clear_black.png');
                            echo anchor("$controller", img($img));
                        ?>
                    </div>
                    <div class="search">
                        <form name='pretraga' action='<?= site_url("$controller/pretraga") ?>' method='POST'>
                            <?php
                                $inputAttr = [
                                    'name' => 'pretraga',
                                    'placeholder' => 'Termin za pretragu',
                                    'class' => 'searchTerm'
                                ];
                                echo form_input($inputAttr);
                                $submitAttr = [
                                    'class' => 'search-submit btn btn-warning',
                                    'name' => 'submit'

                                ];
                                echo form_submit($submitAttr);
                            ?>
                        </form>
                    </div>
                    <div class="login_register">
                        <ul id="log_reg">
                        <li><? echo anchor("$controller/moderatorPanel", "MODERATOR") ?></li>
                        <li><? echo anchor("$controller/logout", "Odjava") ?></li>
                        </ul>
                    </div>
                </div>
                <div class="main_menu_area">
                    <ul id="nav">
                        <li><? echo anchor("$controller/index", "Pocetna") ?></li>
                        <li><? echo anchor("$controller/fudbal", "Fudbal") ?></li>
                        <li><? echo anchor("$controller/kosarka", "Kosarka") ?></li>
                        <li><? echo anchor("$controller/tenis", "Tenis") ?></li>
                        <li><? echo anchor("$controller/f1", "Formula1") ?></li>
                        <li><? echo anchor("$controller/rezultati", "Rezultati") ?></li>
                        <li><? echo anchor("$controller/top10", "Top 10") ?></li>  
                        <li><? echo anchor("$controller/forum", "Forum") ?></li>
                        <li><? echo anchor("$controller/kvizFudbal", "Kviz") ?></li>
                        <li><? echo anchor("$controller/about", "O Nama") ?></li>
                    </ul>
                </div>
                
                