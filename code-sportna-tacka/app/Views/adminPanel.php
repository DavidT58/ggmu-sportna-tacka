<div class="admin-panel">

    <table class="table table-dark">

        <th>idKorisnik</th>
        <th>username</th>
        <th>tip</th>
        <th>BAN</th>
        
        <?php

            foreach ($query->getResult() as $row){
                echo "<tr><td>$row->idKorisnik</td>";
                echo "<td>$row->username</td>";
                
                /* EDIT */

                $options = [
                    '0'  => 'User',
                    '1'    => 'Moderator',
                    '2'  => 'Admin',
                ];

                $select = $row->tip;

                $dropdownButton = [
                    'name'    => 'promeni',
                    'content' => 'Promeni',
                    'class'   => 'btn btn-success',
                    'style'   => 'font-size: 10px',
                    'type'    => 'submit',
                    'value'   => $row->idKorisnik
                ];

                echo "<td>
                    <form method=\"post\" action=\"".site_url("Admin/changeType")."\">"
                    .form_dropdown('type', $options, $select).form_button($dropdownButton).
                    "</form></td>";

                
                /* BAN */
                $banButton = [
                    'name'    => 'ban',
                    'content' => 'BAN',
                    'class'   => 'btn btn-danger',
                    'style'   => 'font-size: 10px',
                    'type'    => 'submit',
                    'value'   => $row->idKorisnik
                ];
                if($row->ban == 1){
                    $banButton['class'] = 'btn btn-primary';
                    $banButton['content'] = 'UNBAN';
                }
                echo "<td><form method=\"post\" action=\"".site_url("Admin/banUser")."\">".form_button($banButton)."</form></td></tr>";
            }
        
        ?>

    </table>

</div>
