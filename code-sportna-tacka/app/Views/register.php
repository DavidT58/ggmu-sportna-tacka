    <form name="registerform" action="<?= site_url("registerSubmit")?>" method="post">
        <div class="container, login">
            <h1 style="text-align: center">Registracija</h1>
            
            <label for="uname"><b>Korisnicko ime</b></label>
            <input type="text" placeholder="Unesite korisnicko ime" value="<?= set_value('username')?>" name="username" id="username">
            
            <label for="birthday">Datum rodjenja</label>
            <input type="date" value="2021-01-01" id="birthday" value="<?= set_value('birthday')?>" name="birthday">

            <label for="email"><b>E-mail adresa</b></label>
            <input type="text" placeholder="Unesite Vasu e-mail adresu" value="<?= set_value('email')?>" name="email" id="email">
        
            <label for="psw"><b>Lozinka</b></label>
            <input type="password" placeholder="Unesite lozinku" value="<?= set_value('password')?>" name="password">
        
            <label for="psw-repeat"><b>Potvrda sifre</b></label>
            <input type="password" placeholder="Ponovite lozinku" value="" name="password-repeat">
            <hr>
        
            <p>Kreiranjem naloga slazete se sa nasim <?= anchor('#', 'pravilima privatnosti')?>.</p>
            <br>
            <?php if(isset($poruka)) echo "<font color='red'>$poruka</font><br>"?>
            <br>
            
            <div>
                <ul>
                    <?php 
                        if(!empty($errors)){
                            foreach ($errors as $error)
                                echo("<li><font color=\"red\"> $error </font></li>");
                        }
                    ?>
                </ul>
            </div>
            <button type="submit" class="registerbtn">Registracija</button>
        </div>
        
        <div class="container signin">
            <p style="font-size: 18px; text-align: center;">Vec imate nalog? 
            <? echo anchor("login", "Prijavite se") ?>.
            </p>
        </div>
    </form>