

<script src="/assets/js/formaZaClanak.js"></script>
<div class="container-fluid">
    <form action="<? echo site_url("$controller/upisiClanakUBazu") ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="upload"/> 
    <div class="row">
        <div class="col-sm-12">
            <table class="table">
                <tr>
                    <td>Unesite naslov članka:</td>
                    <td><input type="text" name="naslov" id="naslov"></td>
                </tr>
                <tr>
                    <td>Unesite tekst članka:</td>
                    <td><textarea name="sadrzaj" id="sadrzaj" cols="60" rows="20"></textarea></td>
                </tr>
                <tr>
                    <td>Izaberite sport za koji želite da postavite kviz?</td>
                    <td>
                        <select name="Sport">
                            <option value="1">Fudbal</option>
                            <option value="2">Košarka</option>
                            <option value="3">Tenis</option>
                            <option value="4">Formula 1</option>
                        </select>
                    </td>
                <tr>
                    <td>Izaberite naslovnu sliku clanka:</td>
                    <td>
                        <label style="position:relative;" for="upload" class="btn btn-dark">
                            Izaberite fajl:
                        </label>
                        <input style="margin-left:-106px;" type="file" name="upload" id="upload">
                    </td>
                    <td><input type='hidden' id='imeFajla' name='imeFajla' value=''/></td>
                </tr>
                <tr>
                    <td colspan='2' align='center'>
                        <input class="btn btn-dark" type="submit" value="Submit" onclick="upisiImeFajla()"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</div>