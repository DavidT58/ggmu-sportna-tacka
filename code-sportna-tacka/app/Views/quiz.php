<script src="/assets/js/kviz.js"></script>

<div class="main_menu_area kviz-nav">
    <ul id="nav1">
        <li><? echo anchor("$controller/kvizFudbal", "Fudbal") ?></li>
        <li><? echo anchor("$controller/kvizKosarka", "Kosarka") ?></li>
        <li><? echo anchor("$controller/kvizTenis", "Tenis") ?></li>
        <li><? echo anchor("$controller/kvizFormula1", "Formula1") ?></li>
    </ul>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <?php
                echo "<h1>$naslovKviza<h1/>";
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form action="" class="">
                <?php
                    $index = 0;
                    $brPitanja = 0;
                    foreach($pitanja as $pitanje) {
                        $brPitanja++;
                        echo "<h2>$pitanje->tekstPitanja</h2><br/>";
                        while (true) {
                            $odgovori = $odgovoriSvi[$index];
                            $index++;
                            echo "<table class='table'>";
                            $brOdgovora = 0;
                            foreach($odgovori as $odgovor) {
                                $brOdgovora++;
                                echo "
                                <tr>
                                    <td style='width:15px;'>
                                        <input type='radio' name='odgovori$index' id='brPitanja".($brPitanja-1)."odg".($brOdgovora-1)."' value='$odgovor->tacan'>
                                    </td>
                                    <td>
                                    <label for='brPitanja".($brPitanja-1)."odg".($brOdgovora-1)."'>$odgovor->tekstOdgovora</label><br/>
                                    </td>
                                </tr>
                                ";
                            }
                            echo "<input type='hidden' id='pitanje".($brPitanja-1)."' value='".($brOdgovora)."'>";
                            echo "</table>";
                            break;
                        }
                    }
                    echo "<input type='hidden' id='brPitanja' value='".$brPitanja."'>";
                ?>
            </form>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-12'>
            <button style='margin-bottom:10px;' class="btn btn-dark" onclick="prikaziRezultat()">Prikaži rezultat</button>
            <label style='margin-left:30px;' id="Rezultat"></label>
        </div>
    </div>
</div>