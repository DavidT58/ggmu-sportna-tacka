<div class='content_area'>

    <div class="main_content floatleft">
        <?php
            echo "<h1 class='clanak-naslov'>".$clanak->naslov."</h1>";
            echo img(array('src' => $clanak->naslovnaSlika, 'class' => 'clanak-slika'));
            echo "<br>";
            echo "<div class='clanak-sadrzaj'>";
            echo nl2br($clanak->sadrzaj);
            echo "</div>";
            echo "<br>";

            $imageProperties =[
                'src'    => '/slikeTemp/avatarIcon.png',
                'class'  => 'slika'
            ];

            echo "<h2 class='title'>Komentari</h2>";
            
        ?>
        
        <div class='komentari'>
            <?php
                foreach($komentari as $komentar){
                    echo "<div class='komentar'>";
                    echo "<div class='korisnik-detalji'>";
                    echo "<div class=''>";
                    echo img($imageProperties);
                    echo "</div>";
                    echo "<div class=''>";
                    echo $komentar['username'];
                    echo "</div></div>";
                    echo "<div class='sadrzaj'>";
                    echo $komentar['sadrzaj'];
                    echo "</div>";
                    
                    
                    echo "<div class='vote-buttons'>";
                    $idKomentar = $komentar['idKomentar'];
                    
                    echo "<form name='votebuttons' action=".site_url("$controller/upvoteComment/$idKomentar")." method='post'>";
                    echo "<button class='btn btn-success'>ʌ</button>";
                    echo form_input('clanak', $clanak->idClanak, "hidden");
                    echo "</form>";
                    
                    echo "<div class='vote-count'>";
                    echo $komentar['brUpvote'] -  $komentar['brDownvote'];
                    echo "</div>";
                    
                    echo "<form name='votebuttons' action=".site_url("$controller/downvoteComment/$idKomentar")." method='post'>";
                    echo form_input('clanak', $clanak->idClanak, "hidden");
                    echo "<button class='btn btn-danger'>v</button>";
                    echo "</form>";
                    echo "</div>";

                    echo "<div class='delete-button'>";
                    if(isset($canDelete)){
                        echo form_open("$controller/obrisiKomentar/$idKomentar");
                        echo form_input('clanak', $clanak->idClanak, "hidden");
                        echo "<button class='btn btn-danger'>X</button>";
                        echo form_close();
                    }
                    echo "</div></div>";
                }
            ?>
        </div>
        <h3>Dodaj komentar</h3>
        <div class='dodaj-komentar'>
            <form name='addcomment' action="<?= site_url("$controller/addComment")?>" method="post" enctype="multipart/form-data">
                <textarea name="sadrzaj" id="sadrzaj" cols="90" rows="10"></textarea>
                <input type='hidden' name='clanak' value='<? print_r($clanak->idClanak) ?>'/>
                <input style="margin-top: 10px;"class="btn btn-dark" type="submit" value="Dodaj Komentar"/>
            </form>
        </div>
    </div>