    
    <form name="loginform" action="<?= site_url("loginSubmit")?>" method="post" class="login">
        <div class="imgcontainer">
            <?
                echo img(array('src' => 'assets/css/images/logo_black_background.svg', 'class' => 'avatar'));
            ?>
        </div>
    
        <div class="container">
            <label for="uname"><b>Username</b></label>
            <input type="text" placeholder="Unesite korisnicko ime" name="username" value="<?= set_value('username')?>">
        
            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Unesite lozinku" name="password" value="<?= set_value('password')?>">
            <?php if(isset($poruka)) echo "<font color='red'>$poruka</font><br>"?>
            <br>
            <div>
                <ul>
                    <?php 
                        if(!empty($errors)){
                            foreach ($errors as $error)
                                echo("<li><font color=\"red\"> $error </font></li>");
                        }
                    ?>
                </ul>
            </div>
            <button type="submit">Prijava</button>
            <label>
                <input type="checkbox" checked="checked" name="remember"> Zapamti me
            </label>
            
        </div>
    
        <div class="container" style="background-color:#f1f1f1">
            <button type="button" id="cancelbtn" onClick="parent.location='index.html'">Ponisti</button>
            <span class="psw" style="text-align: center; font-size: 15px; margin-top: 5px;"><a href="#">Zaboravljena lozinka?</a></span>
        </div>
    </form>