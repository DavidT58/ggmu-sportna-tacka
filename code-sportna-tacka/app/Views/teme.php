<h1 align='center' style="margin-top:25px"> <b> FORUM </b> </h1>
<div class="forum" style="margin:50px">
  <form action="" method="POST"> 
      <table class="table table-light table-striped" style="width:70%; margin:auto"; align='center'>
        <th>Naziv teme</th>
        <?php
          foreach($teme as $tema){
            echo "<tr><td>";
            echo "<div class='forum-tema'>";
            echo anchor("$controller/tema/{$tema->idTema}","".$tema->nazivTeme);
            if(isset($canDelete)){
              // log_message('error', print_r("test", true));
              echo form_open("$controller/obrisiForumTemu/{$tema->idTema}");
              echo form_input('sekcija', $tema->idSekcija, "hidden");
              echo "<button class='btn btn-danger'>X</button>";
              echo form_close();
            }
            echo "</td></tr>";
            echo "</div>";
          }
        ?>
      </table>

      <div class = "button" align='center' style="margin-top:25px">
      <?php echo "<button type='button' class='btn btn-warning'>".anchor("$controller/dodajTemu/{$idSekcija}","Dodaj temu")."</button>"?>
      </div>
  </form>
</div>