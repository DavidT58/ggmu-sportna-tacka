<div class='container'>
    <div class='col-9'>
      <h1 class='title'> Novosti iz sveta fudbala </h1>

      <?php
        $id = 0;
        foreach($clanci as $clanak){
          echo "<div class='col-4 floatleft d-flex justify-content-around'><div class='pocetna-clanci'>";
          $naslov = $clanak->naslov;
          $naslovnaSlika = $clanak->naslovnaSlika;
          echo "<img src='$naslovnaSlika'>";
          echo "<h4>$naslov</h4>";
          $idClanak = $clanak->idClanak;
          $anchorAttributes = [
            'class' => 'procitaj-vise'
          ];
          echo anchor("$controller/clanak/$idClanak", "Procitaj vise ->", $anchorAttributes);
          echo "</div></div>";
        }
      ?>
    </div>
</div>