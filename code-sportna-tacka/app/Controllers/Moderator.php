<?php namespace App\Controllers;

use App\Models\KorisnikModel;
use App\Models\KvizModel;
use App\Models\PitanjeModel;
use App\Models\OdgovorModel;
use App\Models\ClanakModel;
use App\Models\SekcijaModel;
use App\Models\TemaModel;
use App\Models\PostModel;

use Google\Cloud\Storage\StorageClient;

/**
 * Moderator
 * 
 * David Cejtlin 2018/0700
 * Aleksa Janjic 2018/0554
 * 
 * V1.0
 */

class Moderator extends BaseController {


    /**
	 * Funkcija odgovorna za prikaz View-a, sa prosledjenim podacima
	 * @return void
	 * 
	 */
    protected function prikaz($page, $data) {
        $data["controller"] = 'Moderator';
        $data["poruka"] = "poruka";
        $data['canDelete'] = 1;

        echo view('template/header_moderator', $data);
		echo view("$page", $data);
		echo view('template/footer', $data);
    }
	
    /**
     * funkcija koriscena za prikaz forme za postavljanje kviza
     */
    public function postaviNoviKviz() {
        $this->prikaz('formaZaKviz', []);
    }

    /**
     * funkcija korisena za upis kviza u bazu
     * @return void
     */
    public function upisiKvizUBazu() {
        $kvizModel = new KvizModel();
        $pitanjeModel = new PitanjeModel();
        $odgovorModel = new OdgovorModel();
        $selectedSport = $this->request->getVar('Sport');
        $naslovKviza = $this->request->getVar('Naslov');
        $selectedBrP = $this->request->getVar('BrojPitanja');

        $kvizModel->insertKviz($selectedSport, $naslovKviza); // INSERT kviza u bazu
        $sumaOdg = 0;
        for ($i = 0; $i < $selectedBrP; $i++) {
            $tekstPitanja = $this->request->getVar('pitanje'.$i);
            $pitanjeModel->insertPitanje($tekstPitanja, $naslovKviza, $selectedSport);
            $brOdgovora = $this->request->getVar('brOdgZaPitanje'.$i);
            for ($j = 0; $j < $brOdgovora; $j++) {
                $odgovor = $this->request->getVar('odgovor'.$sumaOdg);
                $sumaOdg++;
                $radioButton = $this->request->getVar('radioGrupa'.$i);
                $tacnostOdgovora = 0;
                if ($radioButton == "radioGrupa".$i."odg".$j)
                    $tacnostOdgovora = 1;
                $odgovorModel->insertOdgovore($naslovKviza, $selectedSport, $tekstPitanja, $odgovor, $tacnostOdgovora);
            }
        }
        return redirect()->to(site_url('Moderator'));
    }

    /**
     * funkcija za prikaz forme za postavljanje clanka
     * @return void
     */
    public function postaviClanak() {
        $this->prikaz("formaZaClanak", []);
    }

    /**
     * pomocna funkcija Google Cloud Storage API-ja za uploadovanje slike na Cloud Storage
     * @return void
     */
    function upload_object($objectName, $fileName) {
        $storage = new StorageClient();
        $file = fopen($fileName, 'r');
        $bucket = $storage->bucket("ggmu-sportna-tacka");
        $object = $bucket->upload($file, [
            'name' => $objectName
        ]);
    }

    /**
     * funkcija za upisivanje clanka u bazu podataka
     * @return void
     */
    public function upisiClanakUBazu() {
        $clanakModel = new ClanakModel();

        $naslovClanka = $this->request->getVar("naslov");
        $tekstClanka = $this->request->getVar("sadrzaj");
        $idSport = $this->request->getVar("Sport");
        $korisnik = $this->session->get('korisnik');   
        $clanakModel->insertClanak($idSport, $korisnik['idKorisnik'], $naslovClanka, $tekstClanka);

        $imeFajla = $_FILES["upload"]["tmp_name"];
        $type = $_FILES["upload"]["type"];
        $temp = explode("/", $type);
        $ext = $temp[1];
        $poslednjiClanak = $clanakModel->dohvatiPoslednjiClanak();
        $nazivSlike = $poslednjiClanak->idClanak;
        $file = "$nazivSlike"."."."$ext";
        $clanakModel->postaviNaslovnuSliku($poslednjiClanak->idClanak, "https://storage.googleapis.com/ggmu-sportna-tacka/".$file);
        $this->upload_object("$file", $imeFajla);
        return redirect()->to(site_url('Moderator'));
    }

    /**
     * funkcija za odjavljivanje sa aplikacije
     * @return void
     */
    public function logout(){
        $this->session->destroy();
        return redirect()->to(site_url('/'));
    }

    /**
     * funkcija za prikaz moderatorskog panela
     * @return void
     */
    public function moderatorPanel(){
        return $this->prikaz('moderatorPanel', null);
    }

    /**
     * funkcija za prikaz svih clanaka iz moderatorskog panela
     * @return void
     */
    public function brisanjeClanka(){
        $clanakModel = new ClanakModel();

        $clanci = $clanakModel->findAll();

        return $this->prikaz('brisanjeClanka', ['clanci' => $clanci]);
    }

    /**
     * funkcija koja brise clanak sa zadatim ID-jem
     * @return void
     */
    public function obrisiClanak($idClanak){
        $clanakModel = new ClanakModel();

        $clanakModel->delete($idClanak);

        return redirect()->to(site_url('Moderator/brisanjeClanka'));
    }


}
