<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use App\Models\KvizModel;
use App\Models\OdgovorModel;
use App\Models\PitanjeModel;
use App\Models\KorisnikModel;
use App\Models\ClanakModel;
use App\Models\SekcijaModel;
use App\Models\TemaModel;
use App\Models\PostModel;
use App\Models\KomentarModel;

define('FUDBAL', 1);
define('KOSARKA', 2);
define('TENIS', 3);
define('FORMULA1', 4);


/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

 /**
 * BaseController
 * 
 * David Cejtlin 2018/0700
 * Aleksa Janjic 2018/0554
 * Andrija Lazarevic 2018/0584
 * Pavle Colic 2018/0103
 * 
 * V1.0
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['html', 'url', 'form'];

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();
		$this->session = session();
	}

	/**
	 * Funkcija odgovorna za prikaz View-a, sa prosledjenim podacima
	 * @return void
	 * 
	 */
	protected function prikaz($page, $data){
		throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	}

	/**
	 * Funkcija za prikaz kviza
	 * @return void
	 */
	protected function prikaziKviz($sport) {
        $kvizModel = new KvizModel();
        $odgovorModel = new OdgovorModel();
        $pitanjeModel = new PitanjeModel();

        $kviz = $kvizModel->pronadjiKviz($sport);
        $pitanja = $pitanjeModel->pronadjiPitanja($kviz[count($kviz)-1]->idKviz);
        $index = 0;
        foreach($pitanja as $pitanje) {
            $odgovoriSvi[$index++] = $odgovorModel->pronadjiOdgovore($pitanje->idPitanje);
        }
        $kvizData['naslovKviza'] = $kviz[count($kviz)-1]->naslovKviza;
        $kvizData['pitanja'] = $pitanja;
        $kvizData['odgovoriSvi'] = $odgovoriSvi;
		$this->prikaz('quiz', $kvizData);
    }

	/**
	 * funkcije kviz{sport} koriste se za prikaz kvizova pojedinacno
	 * @return void
	 */
	public function kvizFudbal() {
        $this->prikaziKviz(FUDBAL);
    }

    public function kvizKosarka() {
        $this->prikaziKviz(KOSARKA);
    }

    public function kvizTenis() {
        $this->prikaziKviz(TENIS);
    }

    public function kvizFormula1() {
        $this->prikaziKviz(FORMULA1);
    }

	/**
	 * funkcija se koristi za prikaz pocetne stranice
	 * @return void
	 */
	public function index(){
		$clanakModel = new ClanakModel();
		$clanak = $clanakModel->dohvatiPoslednjeClanke(1, 0)[0];
		$clanciFudbal = $clanakModel->dohvatiPoslednjeClanke(1, 3);
		$clanciKosarka = $clanakModel->dohvatiPoslednjeClanke(2, 3);
		$clanciTenis = $clanakModel->dohvatiPoslednjeClanke(3, 3);
		$clanciF1 = $clanakModel->dohvatiPoslednjeClanke(4, 3);
		// log_message('error', print_r($test, true));
		$this->prikaz('homepage', ["glavniClanak"=>$clanak, "clanciFudbal"=>$clanciFudbal, "clanciKosarka"=>$clanciKosarka, "clanciTenis"=>$clanciTenis, "clanciF1"=>$clanciF1]);
    }

	/**
	 * funkcija koriscena za pretragu
	 * @return void
	 */
	public function pretraga(){
        $clanakModel = new ClanakModel();
        $trazeno = $this->request->getVar('pretraga');
		
		if($trazeno != ''){
			$clanci = $clanakModel->pretraga($trazeno);
			log_message('error', print_r($clanci, true));
			$this->prikaz('rezultatPretrage', ['clanci' => $clanci, 'trazeno' => $trazeno]);
		}
		else{
			log_message('error', print_r("ne radi", true));
			$router = service('router'); 
			$controller  = $router->controllerName();
			$controllerName = explode("\\", $controller)[3];
			return redirect()->to(site_url("$controllerName"));
		}
    }

	/**
	 * funkcija za odjavljivanje korisnika sa aplikacije
	 * @return void
	 */
	public function logout(){
        $this->session->destroy();
        return redirect()->to(site_url('/'));
    }

	/**
	 * funkcija clanak($idClanak) kao parametar prima id clanka koji se prikazuje
	 * @return void
	 */
	public function clanak($idClanak){
		$clanakModel = new ClanakModel();
		$korisnikModel = new KorisnikModel();
		$komentarModel = new KomentarModel();
		$clanak = $clanakModel->find($idClanak);
		$komentari = $komentarModel->dohvatiKomentare($idClanak);
		$ret = [];
		foreach($komentari as $komentar){
			$username = $korisnikModel->find($komentar['idKorisnik'])['username'];
			$komentar['username'] = $username;
			array_push($ret, $komentar);
		}
		// log_message('error', print_r($ret, true));
		if(!$this->session->has('korisnik')){
			$this->prikaz('clanak_guest', ['clanak' => $clanak, 'komentari' => $ret]);
		}
		else{
			$this->prikaz('clanak', ['clanak' => $clanak, 'komentari' => $ret]);
		}
	}

	/**
	 * funkcija se koristi za prikaz foruma
	 * @return void
	 */
	public function forum(){

		$sekcijaModel = new SekcijaModel();
		$sekcije = $sekcijaModel->pretraga();

		$this->prikaz("sekcije", ["sekcije" => $sekcije]);
	}

	/**
	 * funkcija za prikaz View-a za dodavanje teme
	 * @return void
	 */
	public function dodajTemu($idSekcija){
		$this->prikaz("dodajTemu", ["idSekcija" => $idSekcija]);
	}

	/**
	 * funkcija za validiranje unete teme
	 * @return void
	 */
	public function proveraTeme($idSekcija){

		if (!$this->validate(['nazivTeme' => 'required'])) {
			return $this->prikaz('dodajTemu', ['poruka' => "Morate uneti naziv teme", "idSekcija" => $idSekcija]);
		}

		$data = [
			'nazivTeme' => $this->request->getVar("nazivTeme"),
			'idSekcija' => $idSekcija
		];

		$db = \Config\Database::connect();
		$builder = $db->table('ForumTema');
		$builder->insert($data);

		$temaModel = new TemaModel();
		$teme = $temaModel->where("idSekcija", $idSekcija)->findAll();

		// $this->prikaz("teme", ["teme" => $teme, "idSekcija" => $idSekcija]);
		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		return redirect()->to(site_url("$controllerName/sekcija/$idSekcija"));
	}

	/**
	 * funkcija za prikaz View-a za dodavanje posta
	 * @return void
	 */
	public function dodajPost($idTema){

		$this->prikaz("dodajPost", ['idTema' => $idTema]);
	}

	/**
	 * funkcija za validiranje unetog posta
	 * @return void
	 */
	public function proveraPosta($idTema){

		if (!$this->validate(['komentarnatemu' => 'required'])) {
			return $this->prikaz('dodajPost', ['idTema' => $idTema, 'greska' => 'Morate uneti komentar']);
		}

		$korisnik =  $this->session->get('korisnik');
		$username = $korisnik['username'];
		$id =  $korisnik['idKorisnik'];
		// log_message('error', print_r($id, TRUE));

		$data = [
			'idTema' => $idTema,
			'sadrzaj' =>  $this->request->getVar("komentarnatemu"),
			'brUpvote' => '0',
			'brDownvote' => '0',
			'idKorisnik' => $id,
			'username' => $username
		];

		$db = \Config\Database::connect();
		$builder = $db->table('ForumPost');
		$builder->insert($data);

		$postModel = new PostModel();
		$postovi = $postModel->where("idTema", $idTema)->findAll();
		$temamodel = new TemaModel();
		$tema = $temamodel->where('idTema', $idTema)->first();
		$this->prikaz("tema", ["postovi" => $postovi, "tema" => $tema]);
	}

	/**
	 * funkcija kojom se dodaje komentar na clanak u bazu
	 * @return void
	 */
	public function addComment(){
		$komentarModel = new KomentarModel();
		$sadrzaj = $this->request->getVar("sadrzaj");
		$idKorisnik = $this->session->get('korisnik')['idKorisnik'];
		$idClanak = $this->request->getVar("clanak");
		// log_message('error', print_r($idClanak, true));
		$data =[
			'idKorisnik' => $idKorisnik,
			'idClanak' => $idClanak,
			'sadrzaj' => $sadrzaj 
		];

		$komentarModel->insert($data);
		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		// log_message('error', print_r($controllerName, TRUE));
		return redirect()->to(site_url("$controllerName/clanak/$idClanak"));
	}

	/**
	 * funkcija kojom se povecava broj glasova za komentar
	 * @return void
	 */
	public function upvoteComment($idKomentar){
		$komentarModel = new KomentarModel();

		$komentar = $komentarModel->find($idKomentar);

		$numOfUpvotes = $komentar['brUpvote'];
		$numOfUpvotes++;
		$data = [ 'brUpvote' => $numOfUpvotes];
		$komentarModel->update($idKomentar, $data);

		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];

		$idClanak = $this->request->getVar("clanak");
		// log_message('error', print_r($idClanak, TRUE));
		return redirect()->to(site_url("$controllerName/clanak/$idClanak"));
	}

	/**
	 * funkcija kojom se smanjuje broj glasova na komentar
	 * @return void
	 */
	public function downvoteComment($idKomentar){
		$komentarModel = new KomentarModel();

		$komentar = $komentarModel->find($idKomentar);

		$numOfDownvotes = $komentar['brDownvote'];
		$numOfDownvotes++;
		$data = [ 'brDownvote' => $numOfDownvotes];
		$komentarModel->update($idKomentar, $data);
		

		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];

		$idClanak = $this->request->getVar("clanak");
		return redirect()->to(site_url("$controllerName/clanak/$idClanak"));
	}

	/**
	 * funkcija kojom se povecava broj glasova na forum postu
	 * @return void
	 */
	public function upvotePost($idPost){
		$postModel = new PostModel();

		$idTema = $this->request->getVar('tema');

		$postUpvotes = $postModel->find($idPost)->brUpvote;
		$postUpvotes++;
		$data = [ 'brUpvote' => $postUpvotes];
		$postModel->update($idPost, $data);

		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		return redirect()->to(site_url("$controllerName/tema/$idTema"));
	}

	/**
	 * funkcija kojom se smanjuje broj glasova na forum postu
	 */
	public function downvotePost($idPost){
		$postModel = new PostModel();

		$idTema = $this->request->getVar('tema');

		$postDownvotes = $postModel->find($idPost)->brDownvote;
		$postDownvotes++;
		$data = [ 'brDownvote' => $postDownvotes];
		$postModel->update($idPost, $data);

		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		return redirect()->to(site_url("$controllerName/tema/$idTema"));
	}

	/**
	 * funkcija za prikaz teme na forumu (izlistava postove u okviru teme) 
	 * @return void
	 */
	public function tema($idTema){

		$postModel = new PostModel();
		$postovi = $postModel->where("idTema", $idTema)->findAll();
		$temamodel = new TemaModel();
		$tema = $temamodel->where('idTema', $idTema)->first();


		if(!$this->session->has('korisnik')){
			$this->prikaz("tema_guest", ["postovi" => $postovi, "tema" => $tema]);
		}
		else{
			$this->prikaz("tema", ["postovi" => $postovi, "tema" => $tema]);
		}
		
	}

	/**
	 * funkcija za prikaz sekcije na forumu (izlistava teme u okviru sekcije)
	 * @return void
	 */
	public function sekcija($sekcija){

		$temaModel = new TemaModel();
		$teme = $temaModel->where("idSekcija", $sekcija)->findAll();

		if(!$this->session->has('korisnik')){
			$this->prikaz("teme_guest", ["teme" => $teme, "idSekcija" => $sekcija]);
		}
		else{
			$this->prikaz("teme", ["teme" => $teme, "idSekcija" => $sekcija]);
		}
	}

	/**
	 * funkcija za prikaz svih vesti vezanih za fudbal
	 * @return void
	 */
	public function fudbal(){
		$clanakModel = new ClanakModel();

		$clanci = $clanakModel->dohvatiPoslednjeClanke(1, 0);

		$this->prikaz('vestiFudbal', ['clanci' => $clanci]);
	}

	/**
	 * funkcija za prikaz svih vesti vezanih za kosarku
	 * @return void
	 */
	public function kosarka(){
		$clanakModel = new ClanakModel();

		$clanci = $clanakModel->dohvatiPoslednjeClanke(2, 0);

		$this->prikaz('vestiKosarka', ['clanci' => $clanci]);
	}

	/**
	 * funkcija za prikaz svih vesti vezanih za tenis
	 * @return void
	 */
	public function tenis(){
		$clanakModel = new ClanakModel();

		$clanci = $clanakModel->dohvatiPoslednjeClanke(3, 0);

		$this->prikaz('vestiTenis', ['clanci' => $clanci]);
	}

	/**
	 * funkcija za prikaz svih vesti vezanih za formulu 1
	 * @return void
	 */
	public function f1(){
		$clanakModel = new ClanakModel();

		$clanci = $clanakModel->dohvatiPoslednjeClanke(4, 0);

		$this->prikaz('vestiF1', ['clanci' => $clanci]);
	}

	/**
	 * funkcija kojom se brise post sa foruma sa prosledjenim ID-jem
	 * @return void
	 */
	public function obrisiForumPost($idPost){
		$postModel = new PostModel();

		$postModel->delete($idPost);
		$idTema = $this->request->getVar('tema');

		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		return redirect()->to(site_url("$controllerName/tema/$idTema"));
	}

	/**
	 * funkcija kojom se brise tema i svi njeni postovi sa prosledjenim ID-jevima
	 * @return void
	 */
	public function obrisiForumTemu($idTema){
		$temaModel = new TemaModel();
		$temaModel->delete($idTema);

		$idSekcija = $this->request->getVar('sekcija');
		log_message('error', print_r($idSekcija, true));

		$router = service('router');
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];
		return redirect()->to(site_url("$controllerName/sekcija/$idSekcija"));
	}

	/**
	 * funkcija kojom se brise komentar sa Clanka
	 * @return void
	 */
	public function obrisiKomentar($idKomentar){

		$komentarModel = new KomentarModel();

		$komentarModel->delete($idKomentar);


		$router = service('router'); 
		$controller  = $router->controllerName();
		$controllerName = explode("\\", $controller)[3];

		$idClanak = $this->request->getVar("clanak");
		return redirect()->to(site_url("$controllerName/clanak/$idClanak"));
	}

	public function rezultati(){
		$this->prikaz('rezultati',[]);
	}

}
