<?php namespace App\Controllers;

use App\Models\KvizModel;
use App\Models\OdgovorModel;
use App\Models\PitanjeModel;
use App\Models\KorisnikModel;
use App\Models\ClanakModel;
use App\Models\SekcijaModel;
use App\Models\TemaModel;
use App\Models\PostModel;


class User extends BaseController {

    protected function prikaz($page, $data) {
        $data["controller"] = 'User';
        $data['korisnik'] = $this->session->get('korisnik');

        echo view('template/header_user', $data);
        echo view("$page", $data);
		echo view('template/footer', $data);
    }

    public function logout(){
        $this->session->destroy();
        return redirect()->to(site_url('/'));
    }
}