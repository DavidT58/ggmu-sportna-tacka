<?php namespace App\Controllers;


use App\Models\KorisnikModel;
use App\Models\SekcijaModel;
use App\Models\TemaModel;
use App\Models\PostModel;

/**
 * Admin
 * 
 * David Cejtlin 2018/0700
 * 
 * V1.0
 */

class Admin extends BaseController {


    /**
	 * Funkcija odgovorna za prikaz View-a, sa prosledjenim podacima
	 * @return void
	 * 
	 */
    protected function prikaz($page, $data) {
        $data["controller"] = 'Admin';
        $data['canDelete'] = 1;

        echo view('template/header_admin', $data);
		echo view("$page", $data);
		echo view('template/footer', $data);
    }

    /**
     * funkcija koriscena za prikaz administratorskog panela
     */
    public function adminPanel(){
        
        $isAdmin = $this->session->get('korisnik')['tip'];
        // log_message('error', print_r($isAdmin, true));

        switch($isAdmin){
            case 0:
                return redirect()->to(site_url('User'));
            case 1:
                return redirect()->to(site_url('Moderator'));    
        }

        // log_message('error', print_r("lmao", true));

        $db = \Config\Database::connect();

        $query = 'SELECT idKorisnik, username, datumRodjenja, email, tip, ban FROM Korisnik';

        $data = [
            'query' => $db->query($query)
        ];

        $this->prikaz("adminPanel", $data);
    }

    /**
     * funkcija koriscena za oznacavanje korisnika 'banovanim' (zabranjuje mu se prijava na sistem)
     * @return void
     */
    public function banUser(){
        
        $idKorisnikToBan = $this->request->getVar('ban');
        // log_message('error', print_r($requestID, true));

        $korisnikModel = new KorisnikModel();

        $isBanned = $korisnikModel->find($idKorisnikToBan)['ban'];

        log_message('error', print_r($isBanned, true));

        if($isBanned){
            $data = [
                'idKorisnik' => $idKorisnikToBan,
                'ban' => 0
            ];
        }
        else{
            $data = [
                'idKorisnik' => $idKorisnikToBan,
                'ban' => 1
            ];
        }

        $korisnikModel->save($data);

        $this->adminPanel();
    }

    /**
     * funkcija koriscena za menjanje tipa korisnika
     * @return void
     */
    public function changeType(){

        $idKorisnikToMod = $this->request->getVar('promeni');

        $newType = $this->request->getVar('type');

        $korisnikModel = new KorisnikModel();

        $data = [
            'idKorisnik' => $idKorisnikToMod,
            'tip' => $newType
        ];

        $korisnikModel->save($data);

        log_message('error', print_r("ID: ".$idKorisnikToMod, true));
        log_message('error', print_r("Type: ".$newType, true));

        $this->adminPanel();
    }
}
