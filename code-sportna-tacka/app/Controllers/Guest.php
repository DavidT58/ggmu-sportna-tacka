<?php namespace App\Controllers;

use App\Models\KorisnikModel;
use App\Models\ClanakModel;
use App\Models\SekcijaModel;
use App\Models\TemaModel;
use App\Models\PostModel;

/**
 * Guest
 * 
 * David Cejtlin 2018/0700
 * Aleksa Janjic 2018/0554
 * Andrija Lazarevic 2018/0584
 * Pavle Colic 2018/0103
 * 
 * V1.0
 */

class Guest extends BaseController {

    /**
	 * Funkcija odgovorna za prikaz View-a, sa prosledjenim podacima
	 * @return void
	 * 
	 */
    protected function prikaz($page, $data) {
        $data["controller"] = 'Guest';

        echo view('template/header', $data);
		echo view("$page", $data);
		echo view('template/footer', $data);
    }

    /**
     * funkcija koja prikazuje stranicu za prijavu
     * @return void
     */
    public function login($poruka=null){
        $this->prikaz('login', ['poruka' => $poruka]);
    }

    /**
     * funkcija koja prikazuje stranicu za registraciju
     * @return void
     */
    public function register($poruka=null){
        $this->prikaz('register', ['poruka' => $poruka]);
    }

    /**
     * funkcija za validaciju i prijavu korisnika na aplikaciju
     * @return void
     */
    public function loginSubmit(){
        if(!$this->validate([
            'username' => [
                'rules' => 'required',
                'errors' => [
                    'required' => "Korisnicko ime ne sme biti prazno"
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => "Lozinka ne sme biti prazna"
                ]
            ]
            ]))
        {
            return $this->prikaz('login', ['errors' => $this->validator->getErrors()]);
        }
        
        $korisnikModel = new KorisnikModel();

        $requestUsername = $this->request->getVar('username');
        
        //primarni kljuc username umesto idKorisnik?
        $korisnik = $korisnikModel->getByUsername($requestUsername);

        if($korisnik == null)
            return $this->login('Korisnik ne postoji');

        $requestPassword = $this->request->getVar('password');

        if($korisnik['password'] != $requestPassword)
            return $this->login('Pogresna lozinka');

        $this->session->set('korisnik', $korisnik);
        
        switch($korisnik['tip']){
            case 0:
                return redirect()->to(site_url('User'));
                break;
            case 1:
                return redirect()->to(site_url('Moderator'));
                break;
            case 2:
                return redirect()->to(site_url('Admin'));
                break;
        }

        // return redirect()->to(site_url('User'));
        return null;
    }

    /**
     * funkcija za validaciju i registraciju korisnika na aplikaciju
     * @return void
     */
    public function registerSubmit(){

        if(!$this->validate([
            'username' => [
                'rules' => 'required|alpha_numeric',
                'errors' => [
                    'required' => 'Morate izabrati korisnicko ime',
                    'alpha_numeric' => 'Korisnicko ime sme sadrzati samo alfanumericke karaktere'
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[8]',
                'errors' => [
                    'required' => 'Morate izabrati lozinku',
                    'min_length' => 'Lozinka mora biti duza od 8 karaktera'
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Morate uneti mejl',
                    'valid_email' => 'Mejl nije u ispravnom formatu'
                ]
            ],
            'password-repeat' => [
                'rules' => 'required|matches[password]',
                'errors' => [
                    'required' => 'Morate uneti ponovljenu lozinku',
                    'matches' => 'Lozinke se ne poklapaju'
                ]
            ]
            ]))
        {
            return $this->prikaz('register', ['errors' => $this->validator->getErrors()]);
        }

        $requestUsername = $this->request->getVar('username');

        $korisnikModel = new KorisnikModel();
        $existingUser = $korisnikModel->getByUsername($requestUsername);

        if($existingUser != null){
            return $this->register('Korisnicko ime je zauzeto');
        }

        $requestBirthday = $this->request->getVar('birthday');
        $requestEmail = $this->request->getVar('email');
        $requestPassword = $this->request->getVar('password');

        $data = [
            'username'      => $requestUsername,
            'datumRodjenja' => $requestBirthday,
            'email'         => $requestEmail,
            'password'      => $requestPassword,
            'tip'           => 0
        ];

        $korisnikModel->insert($data);
            
        return redirect()->to(site_url('login'));
    }
}