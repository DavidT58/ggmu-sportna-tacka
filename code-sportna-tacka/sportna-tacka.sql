-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: May 28, 2021 at 12:13 AM
-- Server version: 8.0.24
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sportna-tacka`
--
CREATE DATABASE IF NOT EXISTS `sportna-tacka` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `sportna-tacka`;

-- --------------------------------------------------------

--
-- Table structure for table `Clanak`
--

CREATE TABLE `Clanak` (
  `idClanak` int NOT NULL,
  `idSport` int NOT NULL,
  `idKorisnik` int NOT NULL,
  `naslov` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `naslovnaSlika` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ForumPost`
--

CREATE TABLE `ForumPost` (
  `idPost` int NOT NULL,
  `idTema` int NOT NULL,
  `sadrzaj` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brUpvote` int NOT NULL,
  `brDownvote` int NOT NULL,
  `idKorisnik` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ForumSekcija`
--

CREATE TABLE `ForumSekcija` (
  `idSekcija` int NOT NULL,
  `nazivSekcije` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ForumTema`
--

CREATE TABLE `ForumTema` (
  `idTema` int NOT NULL,
  `idSekcija` int NOT NULL,
  `nazivTeme` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Komentar`
--

CREATE TABLE `Komentar` (
  `idKomentar` int NOT NULL,
  `idClanak` int NOT NULL,
  `sadrzaj` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brUpvote` int NOT NULL,
  `brDownvote` int NOT NULL,
  `idKorisnik` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Korisnik`
--

CREATE TABLE `Korisnik` (
  `idKorisnik` int NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datumRodjenja` date NOT NULL,
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tip` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Korisnik`
--

INSERT INTO `Korisnik` (`idKorisnik`, `username`, `datumRodjenja`, `email`, `password`, `tip`) VALUES
(1, 'admin', '2021-05-05', 'admin@admin.com', 'admin', 2),
(2, 'user', '2021-05-01', 'user@user.com', 'user', 0),
(3, 'moderator', '2021-05-11', 'moderator@moderator.com', 'moderator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Kviz`
--

CREATE TABLE `Kviz` (
  `idKviz` int NOT NULL,
  `naslovKviza` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idSport` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Kviz`
--

INSERT INTO `Kviz` (`idKviz`, `naslovKviza`, `idSport`) VALUES
(1, 'Liga šampiona', 1),
(2, 'Premijer liga', 1),
(8, 'La Liga', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Odgovor`
--

CREATE TABLE `Odgovor` (
  `idOdgovor` int NOT NULL,
  `idPitanje` int NOT NULL,
  `tekstOdgovara` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tacan` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Odgovor`
--

INSERT INTO `Odgovor` (`idOdgovor`, `idPitanje`, `tekstOdgovara`, `tacan`) VALUES
(1, 1, 'Lionel Mesi', 0),
(2, 1, 'Kristijano Ronaldo', 1),
(3, 1, 'Karim Benzema', 0),
(4, 1, 'Robert Levandovski', 0),
(5, 2, 'Paolo Maldini', 0),
(6, 2, 'Lionel Mesi', 0),
(7, 2, 'Alfredo Di Stefano', 1),
(8, 2, 'Gerd Miler', 0),
(9, 3, 'Mančester junajted', 1),
(10, 3, 'Liverpul', 0),
(11, 3, 'Arsenal', 0),
(12, 3, 'Čelsi', 0),
(17, 9, 'Real Madrid', 1),
(18, 9, 'Barselona', 0),
(19, 9, 'Atletiko Madrid', 0),
(20, 9, 'Valensija', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Pitanje`
--

CREATE TABLE `Pitanje` (
  `idPitanje` int NOT NULL,
  `idKviz` int NOT NULL,
  `tekstPitanja` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Pitanje`
--

INSERT INTO `Pitanje` (`idPitanje`, `idKviz`, `tekstPitanja`) VALUES
(1, 1, 'Ko je postigao najviše golova u istoriji lige šampione?'),
(2, 1, 'Koji igrač je osvojio najviše titula u Ligi šampioni?'),
(3, 2, 'Koji klub je osvojio najviše titula u istoriji Premijer lige?'),
(9, 8, 'Koji je najtrofejniji klub u istoriji La lige?');

-- --------------------------------------------------------

--
-- Table structure for table `Sport`
--

CREATE TABLE `Sport` (
  `idSport` int NOT NULL,
  `imeSporta` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Sport`
--

INSERT INTO `Sport` (`idSport`, `imeSporta`) VALUES
(1, 'Fudbal'),
(2, 'Kosarka'),
(3, 'Tenis'),
(4, 'Formula 1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Clanak`
--
ALTER TABLE `Clanak`
  ADD PRIMARY KEY (`idClanak`),
  ADD KEY `idSport` (`idSport`),
  ADD KEY `idKorisnik` (`idKorisnik`);

--
-- Indexes for table `ForumPost`
--
ALTER TABLE `ForumPost`
  ADD PRIMARY KEY (`idPost`,`idTema`),
  ADD KEY `idTema` (`idTema`),
  ADD KEY `idKorisnik` (`idKorisnik`);

--
-- Indexes for table `ForumSekcija`
--
ALTER TABLE `ForumSekcija`
  ADD PRIMARY KEY (`idSekcija`);

--
-- Indexes for table `ForumTema`
--
ALTER TABLE `ForumTema`
  ADD PRIMARY KEY (`idTema`,`idSekcija`),
  ADD KEY `idSekcija` (`idSekcija`);

--
-- Indexes for table `Komentar`
--
ALTER TABLE `Komentar`
  ADD PRIMARY KEY (`idKomentar`,`idClanak`),
  ADD KEY `idKorisnik` (`idKorisnik`),
  ADD KEY `idClanak` (`idClanak`);

--
-- Indexes for table `Korisnik`
--
ALTER TABLE `Korisnik`
  ADD PRIMARY KEY (`idKorisnik`);

--
-- Indexes for table `Kviz`
--
ALTER TABLE `Kviz`
  ADD PRIMARY KEY (`idKviz`),
  ADD KEY `idSport` (`idSport`);

--
-- Indexes for table `Odgovor`
--
ALTER TABLE `Odgovor`
  ADD PRIMARY KEY (`idOdgovor`,`idPitanje`),
  ADD KEY `idPitanje` (`idPitanje`);

--
-- Indexes for table `Pitanje`
--
ALTER TABLE `Pitanje`
  ADD PRIMARY KEY (`idPitanje`,`idKviz`),
  ADD KEY `idKviz` (`idKviz`);

--
-- Indexes for table `Sport`
--
ALTER TABLE `Sport`
  ADD PRIMARY KEY (`idSport`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Clanak`
--
ALTER TABLE `Clanak`
  MODIFY `idClanak` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ForumPost`
--
ALTER TABLE `ForumPost`
  MODIFY `idPost` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ForumSekcija`
--
ALTER TABLE `ForumSekcija`
  MODIFY `idSekcija` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ForumTema`
--
ALTER TABLE `ForumTema`
  MODIFY `idTema` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Komentar`
--
ALTER TABLE `Komentar`
  MODIFY `idKomentar` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Korisnik`
--
ALTER TABLE `Korisnik`
  MODIFY `idKorisnik` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Kviz`
--
ALTER TABLE `Kviz`
  MODIFY `idKviz` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Odgovor`
--
ALTER TABLE `Odgovor`
  MODIFY `idOdgovor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `Pitanje`
--
ALTER TABLE `Pitanje`
  MODIFY `idPitanje` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Sport`
--
ALTER TABLE `Sport`
  MODIFY `idSport` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Clanak`
--
ALTER TABLE `Clanak`
  ADD CONSTRAINT `Clanak_ibfk_1` FOREIGN KEY (`idSport`) REFERENCES `Sport` (`idSport`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Clanak_ibfk_2` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `ForumPost`
--
ALTER TABLE `ForumPost`
  ADD CONSTRAINT `ForumPost_ibfk_1` FOREIGN KEY (`idTema`) REFERENCES `ForumTema` (`idTema`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ForumPost_ibfk_2` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `ForumTema`
--
ALTER TABLE `ForumTema`
  ADD CONSTRAINT `ForumTema_ibfk_1` FOREIGN KEY (`idSekcija`) REFERENCES `ForumSekcija` (`idSekcija`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `Komentar`
--
ALTER TABLE `Komentar`
  ADD CONSTRAINT `Komentar_ibfk_1` FOREIGN KEY (`idClanak`) REFERENCES `Clanak` (`idClanak`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Komentar_ibfk_2` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `Kviz`
--
ALTER TABLE `Kviz`
  ADD CONSTRAINT `Kviz_ibfk_1` FOREIGN KEY (`idSport`) REFERENCES `Sport` (`idSport`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `Odgovor`
--
ALTER TABLE `Odgovor`
  ADD CONSTRAINT `Odgovor_ibfk_1` FOREIGN KEY (`idPitanje`) REFERENCES `Pitanje` (`idPitanje`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `Pitanje`
--
ALTER TABLE `Pitanje`
  ADD CONSTRAINT `Pitanje_ibfk_1` FOREIGN KEY (`idKviz`) REFERENCES `Kviz` (`idKviz`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
