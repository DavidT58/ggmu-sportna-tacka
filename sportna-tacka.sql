-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jun 06, 2021 at 05:52 PM
-- Server version: 8.0.24
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sportna-tacka`
--
CREATE DATABASE IF NOT EXISTS `sportna-tacka` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `sportna-tacka`;

-- --------------------------------------------------------

--
-- Table structure for table `Clanak`
--

CREATE TABLE IF NOT EXISTS `Clanak` (
  `idClanak` int NOT NULL AUTO_INCREMENT,
  `idSport` int NOT NULL,
  `idKorisnik` int NOT NULL,
  `naslov` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sadrzaj` text COLLATE utf8_unicode_ci,
  `datum` date DEFAULT NULL,
  `naslovnaSlika` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idClanak`),
  KEY `Clanak_idSport_foreign` (`idSport`),
  KEY `Clanak_idKorisnik_foreign` (`idKorisnik`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Clanak`
--

INSERT INTO `Clanak` (`idClanak`, `idSport`, `idKorisnik`, `naslov`, `sadrzaj`, `datum`, `naslovnaSlika`) VALUES
(1, 1, 2, 'Real se trese! Zidan prozvao čelnike kraljevskog kluba, oni ga podsetili da su mu oprostili kaznu od 10 MILIONA EVRA!', 'Zinedin Zidan i Real Madrid su se rastali, u međuvremenu je na \"Bernabeu\" stigao novi trener, iako sa starim nisu raščišćeni svi računi.\r\n\r\nNakon Zidanovog odlaska nisu ostali trofeji, ali očigledno jesu problemi i težak zadatak za njegovog naslednika, ali i priča oko deset miliona evra!\r\n\r\nNaime, doskorašnji trener kraljevskog kluba je nedavno kritikovao upravu, što je izazvalo bes kod prvog čoveka Florentina Pereza, naročito sada kada se saznalo da je klub legendarnom Francuzu oprostio kaznu od 10.000.000 evra.', '2021-06-01', 'https://storage.googleapis.com/ggmu-sportna-tacka/1.webp'),
(2, 1, 2, 'U Zvezdi ga čekali kao BOMBASTIČNO POJAČANJE, on konačno dolazi u Evropu, ali ne sleće u Beograd - nego u Moskvu!', 'Iskusni nadapač Marko Arnautović nalazi se na korak do prelaska u novi klub, Dinamo iz Moskve.\r\n\r\nKako piše ruski portal \'Čempionat\' austrijski reprezentativac već duže vreme pregovara sa Dinamom, a ovoj klub ga vidi kao zamenu za Nikolaja Kolmičenka, koji je dogovorio transfer u Rostov.\r\n\r\n\"U ovom trenutku obe strane su blizu dogovora\", navodi pomenuti portal uz dodatak da igrač kineske ekipe Šangaj SIPG želi da napusti ovaj tim i vrati se u Evropu.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/2.webp'),
(3, 1, 2, 'PONOVO SE JAVIO NEMANJA VIDIĆ! I protresao Srbiju, bivši fudbaler otvoreno o borbi za predsednika FSS', 'Legendarni srpski fudbaler Nemanja Vidić se ponovo oglasio povodom situacije u domaćem fudbalu.\r\n\r\nVeć mesecima su velike turbulencije u Fudbalskom savezu Srbije, i to na samom vrhu, gde bi deo javnosti voleo da vidi upravo Vidića.\r\n\r\nNekadašnji kapiten Mančester junajted je prokomentarisao takvu mogućnost, ali sa akcentom na to da trenutni kandidati ne bi trebalo da dobiju mesto predsednika FS Srbije.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/3.webp'),
(4, 2, 2, '\"Ama, svi navijamo da Nikola Jokić bude MVP!\" Bogdan Bogdanović otkrio američkim novinarima KAKO SE DRUŽE NBA SRBI!', 'Čitava Srbija ove sezone pažljivo prati dešavanja u NBA ligi, a glavni krivac za to je pre svih Nikola Jokić, koji sjajno igra u Denveru i koji je po mnogim parametrima glavni kandidat za MVP priznanje, kada je prvi deo sezone u pitanju.\r\n\r\nUkoliko bi prema očekivanjima osvojio tu nagradu on bi postao prvi Srbin ikada kom je nešto tako pošlo za rukom, a podršku mu daju svi naši igrači u NBA.\r\n\r\nMeđu njima je i Bogdan Bogdanović, koji je u razgovoru s američkim novinarima iskoristio priliku da im ukaže ko bi trebalo da bude MVP.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/4.webp'),
(5, 2, 2, 'Zvezda i Partizan ŽELE ISTO POJAČANJE! Srbin koji se nije snašao u NBA ligi na meti \"večtih\" rivala!', 'Beogradski \"večiti\" rivali, Crvena zvezda i Partizan, u potrazi su za adekvatnim pojačanjima za novu sezonu, iako je ova tek sinoć za jedne završena, a drugi čekaju nastavak serije s Borcem.\r\n\r\nKako prenose brojni domaći mediji, i crveno i crno-beli su zainteresovani za dovođenje Alena Smailagića, koji nije uspeo da se snađe u NBA ligi i izbori za veću minutažu u Golden Stejt Voriorsima, pa bi zbog toga tokom leta mogao da napusti najjaču ligu na svetu.\r\n\r\nPokazivao je Smailagić svoj raskošan talenat u razvojnoj ligi, nastupajući za Santa Kruz, ali bi vrlo lako mogao da \"bukira\" avionsku kartu za Srbiju umesto povratka u ovaj tim, jer iako ima ugovor sa Voriorsima do 2023. godine, on je garantovan u prve dve sezone, što znači da od kluba zavisi hoće li ga verifikovati za dalje.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/5.webp'),
(6, 2, 2, 'Košarkaški skandal trese Bosnu! Tri tima odustala od borbe za titulu - ČETVRTI PROGLAŠEN ŠAMPIONOM, a haos tek počinje!', 'Novi šampion Bosne i Hercegovine je ekipa Širokog Brijega, koja je do titule došla svom parketu, ali bez minuta na terenu.\r\n\r\nNaime, košarkaši Igokee se nisu pojavili na utakmici fajnl-fora u Širokom Brijegu, a prethodno nije odigran ni susret Borac - Spars, te je za \"zelenim stolom\" odlučeno da se zvaničnim prvakom proglasi domaćin ovog završnog turnira.\r\n\r\nOvo je epilog višemesečnih problema u tamošnjoj košarci i nekoliko promena pravila. Sve je krenulo u aprilu, kada su predstavnici Igokee i Sparsa uputili inicijativu UO KSBiH da se ove godine liga skrati i posle Lige 6 umesto plej-ofa odigra fajnal-for, čiji bi organizator bila prvoplasirana ekipa ligaškog dela, što je i prihvaćeno.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/6.webp'),
(7, 3, 2, 'ŠOK, UDARNA VEST IZ FRANCUSKE Rodžer Federer se povukao sa Rolan Garosa, ništa od duela sa Novakom!', 'Švajcarski teniser Rodžer Federer se povukao sa Rolan Garosa nakon pobede u trećem kolu.\r\n\r\nFederer je trebalo da u osmini finala igra protiv Matea Beretinija, ali je rešio da ne izađe na teren te će Italijan u četvrtfinale bez borbe.\r\n\r\nIskusni Švajcarac je odlučio da završi turnir nakon isrcpljujućeg meča protiv Dominika Kepfera koji je trajao više od tri i po sata.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/7.webp'),
(8, 3, 2, 'DOSAD NEOBJAVLJENI SNIMCI! Pre pet godina Novak Đoković je jedini put osvojio Rolan Garos, a evo šta je potom usledilo! ', 'Novak Đoković je samo jednom u karijeri osvojio Rolan Garos, i to baš na ovaj dan.\r\n\r\nPre pet godina Nole je pobedio u finalu Endija Mareja sa 3:6, 6:1, 6:2, 6:4 i tako po prvi put postao šampion Otvorenog prvenstva Francuske.\r\n\r\nPrvi put, nadamo se ne i jedini, a baš na petogodišnjicu tog istorijskog uspeha, jer je Đoković tada postao vladar sva četiri grend slem turnira, obelodanjeni su snimci koji dosad nisu bili dostupni javnosti.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/8.webp'),
(9, 4, 2, 'F1: Ništa od Singapura, u opticaju nekoliko zamena', 'Velika nagrada Singapura neće se održati ove godine, a u Formuli 1 razmatraju gde bi mogla da se održi trka koja će je zameniti.\r\n\r\nF1 i vlasti u Singapuru saglasili su se da trenutno nije moguće održati trku zbog restrikcija u vezi sa dolascima u zemlji jugoistočme Azije.\r\n\r\nKao potencijalne zamene navode se Turska, Kina, kao i druga trka u Ostinu (SAD). U zavisnosti od izbora domaćina, odrediće se i tačni datumi održavanja celokupnog programa.\r\n\r\n„Nastavljamo rad sa svim promoterima i imamo veliki broj opcija koje bismo mogli da prilagodimo rasporedu ukoliko je to potrebno„, saopštio je predstavnik za medije Formule 1.\r\n\r\nVelika nagrada Singapura bila je zakazana za vikend 1-3. oktobra. Pored njenog otkazivanja, i dalje su pod upitnikom trke u Japanu, Brazilu, Meksiku i Australiji.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/9.jpeg'),
(10, 4, 2, 'Lekleru pol u Bakuu posle haosa u finišu', 'Vozač Ferarija Šarl Lekler osvojio je pol poziciju pred trku za Veliku nagradu Azerbejdžana.\r\n\r\nTo je druga pol pozicija za vozača iz Monaka zaredom, pošto je pre dve nedelje stigao do pola i u Monaku.\r\n\r\nDrugi je kvalifikacije završio Luis Hamilton, a treći je bio Maks Ferštapen.\r\n\r\nPrvih 10 kompletirali su Pjer Gasli, Karlos Sains, Lando Noris, Serhio Peres, Juki Cunoda, Fernando Alonso, Valteri Botas.\r\n\r\nKvalifikacije su obeležili incidenti, Rikardo i Đovinaci su prvo udarili u zid, a nakon toga, pred sam finiš u zid u udarili i Cunoda i Sains u trećoj krivini, nakon čega niko nije mogao da popravi vreme.', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/10.jpeg'),
(11, 4, 2, 'Fetel: Podijum – to zaista nismo očekivali', 'Vozač Aston Martina Sebastijan Fetel bio je izuzetno zadovoljan posle trke za Veliku nagradu Azerbejdžana.\r\n\r\nČetvorostruki šampion sveta je trku u Bakuu završio na drugoj poziciji. Nemac se prvi put našao na podijumu kao vozač Aston Martina.\r\n\r\n„Ovo je veliki uspeh za tim. Imali smo dobar start, pazili smo na gume. Zaista smo imali dobar tempo, što je bilo fantastično“, rekao je Fetel.\r\n\r\n„Bio je ovo sjajan dan u celini. Bilo je sjajno. Presrećan sam. Podijum – to zaista nismo očekivali“.\r\n\r\nPobedu u nedelju je ostvario Serhio Peres, dok je na trećem mestu bio Pjer Gasli.\r\n\r\nU generalnom plasmanu vodi Maks Ferstapen sa 105 bodova, Luis Hamilton ima četiri manje.\r\n\r\nOvogodišnji šampionat Formule 1 nastavlja se Velikom nagradom Francuske 20. juna.\r\n\r\n', '2021-06-06', 'https://storage.googleapis.com/ggmu-sportna-tacka/11.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `ForumPost`
--

CREATE TABLE IF NOT EXISTS `ForumPost` (
  `idPost` int NOT NULL AUTO_INCREMENT,
  `idTema` int NOT NULL,
  `sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `idKorisnik` int NOT NULL,
  `brUpvote` int NOT NULL DEFAULT '0',
  `brDownvote` int NOT NULL DEFAULT '0',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idPost`),
  KEY `ForumPost_idTema_foreign` (`idTema`),
  KEY `ForumPost_idKorisnik_foreign` (`idKorisnik`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ForumPost`
--

INSERT INTO `ForumPost` (`idPost`, `idTema`, `sadrzaj`, `idKorisnik`, `brUpvote`, `brDownvote`, `username`) VALUES
(1, 1, 'Verstappen osvaja', 2, 1, 0, 'moderator'),
(2, 1, 'Hamilton mora u penziju', 3, 1, 0, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `ForumSekcija`
--

CREATE TABLE IF NOT EXISTS `ForumSekcija` (
  `idSekcija` int NOT NULL AUTO_INCREMENT,
  `nazivSekcije` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idSekcija`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ForumSekcija`
--

INSERT INTO `ForumSekcija` (`idSekcija`, `nazivSekcije`) VALUES
(1, 'Pravila foruma'),
(2, 'Fudbal'),
(3, 'Kosarka'),
(4, 'Tenis'),
(5, 'Formula 1');

-- --------------------------------------------------------

--
-- Table structure for table `ForumTema`
--

CREATE TABLE IF NOT EXISTS `ForumTema` (
  `idTema` int NOT NULL AUTO_INCREMENT,
  `idSekcija` int NOT NULL,
  `nazivTeme` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idTema`),
  KEY `ForumTema_idSekcija_foreign` (`idSekcija`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ForumTema`
--

INSERT INTO `ForumTema` (`idTema`, `idSekcija`, `nazivTeme`) VALUES
(1, 5, 'AZERBAIJAN GP');

-- --------------------------------------------------------

--
-- Table structure for table `Komentar`
--

CREATE TABLE IF NOT EXISTS `Komentar` (
  `idKomentar` int NOT NULL AUTO_INCREMENT,
  `idClanak` int NOT NULL,
  `idKorisnik` int NOT NULL,
  `sadrzaj` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `brUpvote` int NOT NULL DEFAULT '0',
  `brDownvote` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`idKomentar`),
  KEY `Komentar_idClanak_foreign` (`idClanak`),
  KEY `Komentar_idKorisnik_foreign` (`idKorisnik`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Komentar`
--

INSERT INTO `Komentar` (`idKomentar`, `idClanak`, `idKorisnik`, `sadrzaj`, `brUpvote`, `brDownvote`) VALUES
(1, 11, 2, 'Sjajna vest!', 0, 0),
(2, 11, 2, 'Napred Vettel! Ua hamilton!', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Korisnik`
--

CREATE TABLE IF NOT EXISTS `Korisnik` (
  `idKorisnik` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `datumRodjenja` date DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tip` int NOT NULL DEFAULT '0',
  `ban` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`idKorisnik`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Korisnik`
--

INSERT INTO `Korisnik` (`idKorisnik`, `username`, `datumRodjenja`, `email`, `password`, `tip`, `ban`) VALUES
(1, 'admin', '2021-01-01', 'admin@admin.com', 'admin', 2, 0),
(2, 'moderator', '2021-01-01', 'moderator@moderator.com', 'moderator', 1, 0),
(3, 'user', '2021-01-01', 'user@user.com', 'user', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Kviz`
--

CREATE TABLE IF NOT EXISTS `Kviz` (
  `idKviz` int NOT NULL AUTO_INCREMENT,
  `idSport` int NOT NULL,
  `naslovKviza` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idKviz`),
  KEY `Kviz_idSport_foreign` (`idSport`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` int NOT NULL,
  `batch` int UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(167, '2021-05-31-224145', 'App\\Database\\Migrations\\Korisnik', 'default', 'App', 1623000327, 1),
(168, '2021-05-31-224150', 'App\\Database\\Migrations\\Sport', 'default', 'App', 1623000327, 1),
(169, '2021-06-03-193948', 'App\\Database\\Migrations\\Clanak', 'default', 'App', 1623000327, 1),
(170, '2021-06-04-201749', 'App\\Database\\Migrations\\Komentar', 'default', 'App', 1623000327, 1),
(171, '2021-06-05-191450', 'App\\Database\\Migrations\\ForumSekcija', 'default', 'App', 1623000327, 1),
(172, '2021-06-05-221538', 'App\\Database\\Migrations\\ForumTema', 'default', 'App', 1623000327, 1),
(173, '2021-06-05-221952', 'App\\Database\\Migrations\\ForumPost', 'default', 'App', 1623000327, 1),
(174, '2021-06-05-223648', 'App\\Database\\Migrations\\Kviz', 'default', 'App', 1623000327, 1),
(175, '2021-06-05-223710', 'App\\Database\\Migrations\\Pitanje', 'default', 'App', 1623000327, 1),
(176, '2021-06-05-223728', 'App\\Database\\Migrations\\Odgovor', 'default', 'App', 1623000327, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Odgovor`
--

CREATE TABLE IF NOT EXISTS `Odgovor` (
  `idOdgovor` int NOT NULL AUTO_INCREMENT,
  `idPitanje` int NOT NULL,
  `tekstOdgovora` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tacan` int NOT NULL,
  PRIMARY KEY (`idOdgovor`),
  KEY `Odgovor_idPitanje_foreign` (`idPitanje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Pitanje`
--

CREATE TABLE IF NOT EXISTS `Pitanje` (
  `idPitanje` int NOT NULL AUTO_INCREMENT,
  `idKviz` int NOT NULL,
  `tekstPitanja` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idPitanje`),
  KEY `Pitanje_idKviz_foreign` (`idKviz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Sport`
--

CREATE TABLE IF NOT EXISTS `Sport` (
  `idSport` int NOT NULL AUTO_INCREMENT,
  `imeSporta` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idSport`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Sport`
--

INSERT INTO `Sport` (`idSport`, `imeSporta`) VALUES
(1, 'Fudbal'),
(2, 'Kosarka'),
(3, 'Tenis'),
(4, 'Formula1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Clanak`
--
ALTER TABLE `Clanak`
  ADD CONSTRAINT `Clanak_idKorisnik_foreign` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`),
  ADD CONSTRAINT `Clanak_idSport_foreign` FOREIGN KEY (`idSport`) REFERENCES `Sport` (`idSport`);

--
-- Constraints for table `ForumPost`
--
ALTER TABLE `ForumPost`
  ADD CONSTRAINT `ForumPost_idKorisnik_foreign` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ForumPost_idTema_foreign` FOREIGN KEY (`idTema`) REFERENCES `ForumTema` (`idTema`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ForumTema`
--
ALTER TABLE `ForumTema`
  ADD CONSTRAINT `ForumTema_idSekcija_foreign` FOREIGN KEY (`idSekcija`) REFERENCES `ForumSekcija` (`idSekcija`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Komentar`
--
ALTER TABLE `Komentar`
  ADD CONSTRAINT `Komentar_idClanak_foreign` FOREIGN KEY (`idClanak`) REFERENCES `Clanak` (`idClanak`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Komentar_idKorisnik_foreign` FOREIGN KEY (`idKorisnik`) REFERENCES `Korisnik` (`idKorisnik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Kviz`
--
ALTER TABLE `Kviz`
  ADD CONSTRAINT `Kviz_idSport_foreign` FOREIGN KEY (`idSport`) REFERENCES `Sport` (`idSport`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Odgovor`
--
ALTER TABLE `Odgovor`
  ADD CONSTRAINT `Odgovor_idPitanje_foreign` FOREIGN KEY (`idPitanje`) REFERENCES `Pitanje` (`idPitanje`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Pitanje`
--
ALTER TABLE `Pitanje`
  ADD CONSTRAINT `Pitanje_idKviz_foreign` FOREIGN KEY (`idKviz`) REFERENCES `Kviz` (`idKviz`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
